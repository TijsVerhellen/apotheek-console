﻿using System;
using System.IO;

namespace Baaziz_Amine_Verhellen_Tijs_ProjectModule1
{
    //GESCHREVEN DOOR AMINE BAAZIZ EN TIJS VERHELLEN

    class Program
    {
        static void Main(string[] args)
        {
            // declaratie arrays en variabelen
            // medicijnenlijst
            int aantalMedicijnen;
            int indexID;
            int[] medID;
            string[] medNaam;
            string[] medType;
            int[] medStock;
            double[] medBarcode;
            double[] medAKP;
            double[] medVKP;
            double[] medTemp;
            string[] medLev;
            bool[] medVS;
            bool[] medTB;
            // menunavigatie
            bool moetAfsluiten = false;
            bool reservatieMenu;
            int submenu;
            int resSubmenu;
            // reservatielijst
            int aantalReservaties;
            string[] resKlant;
            int[] resMedID;
            int[] resAantal;
            DateTime[] resDatum;

            // initialisatie

            // consolevenster aanpassen
            Console.SetWindowSize(200, 50);
            Console.Title = "APOTHEEK JEF: STOCKDATABASE";

            // initialiseer arrays vanuit medicijnen.txt
            if (File.Exists("reservaties.txt"))
            {
                // als reservaties.txt bestaat, lees uit en sla op in arrays
                try
                {
                    string toRead;
                    using (StreamReader lezen = new StreamReader("reservaties.txt"))
                    {
                        // lijn 1 in de txt bevat enkel het totaal aantal reservaties
                        toRead = lezen.ReadLine();
                        aantalReservaties = Convert.ToInt32(toRead/*.Substring(0, 4).Replace(" ", "")*/);
                        // initialisatie op de juiste grootte
                        resKlant = new string[aantalReservaties];
                        resMedID = new int[aantalReservaties];
                        resAantal = new int[aantalReservaties];
                        resDatum = new DateTime[aantalReservaties];

                        for (int i = 0; i < aantalReservaties; i++)
                        {
                            // elke lijn heeft dezelfde format, haal de relevante info uit de string
                            toRead = lezen.ReadLine();
                            resKlant[i] = toRead.Substring(0, 25).Replace(" ", "");
                            resMedID[i] = Convert.ToInt32(toRead.Substring(26, 4).Replace(" ", ""));
                            resAantal[i] = Convert.ToInt32(toRead.Substring(31, 4).Replace(" ", ""));
                            resDatum[i] = new DateTime(Convert.ToInt32(toRead.Substring(42, 4)), Convert.ToInt32(toRead.Substring(39, 2)), Convert.ToInt32(toRead.Substring(36, 2)));
                        }
                    }
                }
                catch
                {
                    // geen crash bij file corruption
                    aantalReservaties = 0;
                    resKlant = new string[0];
                    resMedID = new int[0];
                    resAantal = new int[0];
                    resDatum = new DateTime[0];
                }
            }
            else
            {
                // geen crash als de txt niet gevonden wordt, laat toe om toch nieuwe info op te slaan
                aantalReservaties = 0;
                resKlant = new string[0];
                resMedID = new int[0];
                resAantal = new int[0];
                resDatum = new DateTime[0];
            }

            // idem als uitlezen reservaties.txt
            if (File.Exists("medicijnen.txt"))
            {
                string toRead;
                try
                {
                    using (StreamReader lezen = new StreamReader("medicijnen.txt"))
                    {
                        // lijn 1 in de txt bevat enkel het totaal aantal medicijnen
                        toRead = lezen.ReadLine();
                        aantalMedicijnen = Convert.ToInt32(toRead.Substring(0, 4).Replace(" ", ""));
                        indexID = Convert.ToInt32(toRead.Substring(5, 4).Replace(" ", ""));
                        medID = new int[aantalMedicijnen];
                        medNaam = new string[aantalMedicijnen];
                        medType = new string[aantalMedicijnen];
                        medLev = new string[aantalMedicijnen];
                        medStock = new int[aantalMedicijnen];
                        medBarcode = new double[aantalMedicijnen];
                        medAKP = new double[aantalMedicijnen];
                        medVKP = new double[aantalMedicijnen];
                        medTemp = new double[aantalMedicijnen];
                        medVS = new bool[aantalMedicijnen];
                        medTB = new bool[aantalMedicijnen];

                        for (int i = 0; i < aantalMedicijnen; i++)
                        {
                            // elke lijn heeft dezelfde format
                            toRead = lezen.ReadLine();
                            medID[i] = Convert.ToInt32(toRead.Substring(0, 4).Replace(" ", ""));
                            medNaam[i] = toRead.Substring(5, 25).Replace(" ", "");
                            medType[i] = toRead.Substring(31, 25).Replace(" ", "");
                            medLev[i] = toRead.Substring(57, 25).Replace(" ", "");
                            medStock[i] = Convert.ToInt32(toRead.Substring(83, 10).Replace(" ", ""));
                            medBarcode[i] = Convert.ToDouble(toRead.Substring(94, 13).Replace(" ", ""));
                            medAKP[i] = Convert.ToDouble(toRead.Substring(108, 8).Replace(" ", ""));
                            medVKP[i] = Convert.ToDouble(toRead.Substring(117, 8).Replace(" ", ""));
                            medTemp[i] = Convert.ToDouble(toRead.Substring(126, 6).Replace(" ", ""));
                            medVS[i] = Convert.ToBoolean(toRead.Substring(133, 5).Replace(" ", ""));
                            medTB[i] = Convert.ToBoolean(toRead.Substring(139, 5).Replace(" ", ""));
                        }
                    }
                }
                catch
                {
                    // geen crash bij file corruption
                    aantalMedicijnen = 0;
                    indexID = 0;
                    medID = new int[0];
                    medNaam = new string[0];
                    medType = new string[0];
                    medLev = new string[0];
                    medStock = new int[0];
                    medBarcode = new double[0];
                    medAKP = new double[0];
                    medVKP = new double[0];
                    medTemp = new double[0];
                    medVS = new bool[0];
                    medTB = new bool[0];
                }
            }
            else
            {
                // geen crash als de txt niet gevonden wordt
                aantalMedicijnen = 0;
                indexID = 0;
                medID = new int[0];
                medNaam = new string[0];
                medType = new string[0];
                medLev = new string[0];
                medStock = new int[0];
                medBarcode = new double[0];
                medAKP = new double[0];
                medVKP = new double[0];
                medTemp = new double[0];
                medVS = new bool[0];
                medTB = new bool[0];
            }

            // login vereist me gespecifieerde username en passcode, herhaalt tot het juist is (met foutmelding)
            while (!Login("JEFKE", "JEFKE"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Foute username of wachtwoord!\nDruk op Enter en probeer opniew.");
                Console.ReadLine();
                Console.ResetColor();
            }

            // hoofdmenu
            while (!moetAfsluiten)
            {
                // zolang de optie "afsluiten" niet gekozen is, blijf in het menu
                // navigatie van het hoofdmenu en overgang naar submenus

                // header
                Console.Clear();
                Console.WriteLine("WELKOM BIJ DE DATABASE VAN APOTHEEK JEF\n" +
                    "Dit is het hoofdmenu. Gebruik de \"up\" en \"down\" toetsen om het gewenste menu te selecteren en druk dan op Enter, of geef het getal van het gewenste menu in.\n");
                // keuzemenu met 7 opties, submenu slaat de keuze van de gebruiker op
                submenu = MenuSelectie(7, "nieuw medicijn toevoegen", "bestaand medicijn wijzigen", "bestaand medicijn verwijderen", "bestaand medicijn tonen", "lijst van medicijnen tonen", "reservaties", "applicatie afsluiten");
                Console.Clear();
                switch (submenu)
                {
                    case 1:
                        // medicijn toevoegen
                        NieuwMedicijn(ref aantalMedicijnen, ref indexID, ref medID, ref medNaam, ref medType, ref medLev, ref medStock, ref medBarcode, ref medAKP, ref medVKP, ref medTemp, ref medVS, ref medTB);
                        break;
                    case 2:
                        // medicijn wijzigen
                        WijzigMedicijn(aantalMedicijnen, ref medID, ref medNaam, ref medType, ref medLev, ref medStock, ref medBarcode, ref medAKP, ref medVKP, ref medTemp, ref medVS, ref medTB);
                        break;
                    case 3:
                        // medicijn verwijderen
                        VerwijderMedicijn(ref aantalMedicijnen, ref medID, ref medNaam, ref medType, ref medLev, ref medStock, ref medBarcode, ref medAKP, ref medVKP, ref medTemp, ref medVS, ref medTB, resMedID);
                        break;
                    case 4:
                        // 1 medicijn tonen
                        DisplayMedicijn(medID, medNaam, medType, medLev, medStock, medBarcode, medAKP, medVKP, medTemp, medVS, medTB);
                        break;
                    case 5:
                        // alle medicijnen tonen
                        DisplayAlleMedicijnen(aantalMedicijnen, medID, medNaam, medType, medLev, medStock, medBarcode, medAKP, medVKP, medTemp, medVS, medTB);
                        Console.WriteLine("\nDRUK OP ENTER OM TERUG TE KEREN ");
                        Console.ReadLine();
                        break;
                    case 6:
                        // we zitten nu in een submenu
                        reservatieMenu = true;
                        while (reservatieMenu)
                        {
                            // while loop heeft dezelfde functie als die van het hoofdmenu: blijf in het submenu tot de user er uit wil

                            // header
                            Console.Clear();
                            Console.WriteLine("RESERVATIES\n" +
                                "Dit is het reservatiemenu. Gebruik de \"up\" en \"down\" toetsen om het gewenste menu te selecteren en druk dan op Enter, of geef het getal van het gewenste menu in.\n");
                            // keuzemenu met 5 opties
                            resSubmenu = MenuSelectie(6, "nieuwe reservatie", "reservatie afhalen", "reservatie annuleren", "reservatielijst", "reservaties van een klant", "keer terug");
                            switch (resSubmenu)
                            {
                                case 1:
                                    // nieuwe reservatie
                                    Console.Clear();
                                    ReservatieAanmaken(ref aantalReservaties, ref resKlant, ref resMedID, ref resAantal, ref resDatum, ref medID, ref medStock);
                                    break;
                                case 2:
                                    // afwerken = klant komt artikelen afhalen
                                    Console.Clear();
                                    ReservatieAfwerken(ref aantalReservaties, ref resKlant, ref resMedID, ref resAantal, ref resDatum, medID, medNaam, medType);
                                    break;
                                case 3:
                                    // annuleren = klant komt artikelen niet halen
                                    Console.Clear();
                                    ReservatieAnnuleren(ref aantalReservaties, ref resKlant, ref resMedID, ref resAantal, ref resDatum, ref medStock, medID, medNaam, medType);
                                    break;
                                case 4:
                                    // toon alle reservaties
                                    Console.Clear();
                                    DisplayReservaties(aantalReservaties, resKlant, resMedID, resAantal, resDatum, medID, medNaam, medType);
                                    Console.WriteLine("\nDRUK OP ENTER OM TERUG TE KEREN");
                                    Console.ReadLine();
                                    break;
                                case 5:
                                    // toon alle reservaties van 1 klant
                                    Console.Clear();
                                    DisplayKlant(aantalReservaties, resKlant, resMedID, resAantal, resDatum, medID, medNaam, medType);
                                    Console.WriteLine("\nDRUK OP ENTER OM TERUG TE KEREN");
                                    Console.ReadLine();
                                    break;
                                default:
                                    // keer terug naar het hoofdmenu
                                    reservatieMenu = false;
                                    break;
                            }
                        }
                        break;
                    case 7:
                        // Wegschrijven naar bestanden en afsluiten
                        SaveMedicijnen(aantalMedicijnen, indexID, medID, medNaam, medType, medLev, medStock, medBarcode, medAKP, medVKP, medTemp, medVS, medTB);
                        SaveReservaties(aantalReservaties, resKlant, resMedID, resAantal, resDatum);
                        moetAfsluiten = true;
                        break;
                }
            }
        }

        public static bool Login(string username, string password)
        {
            // bool voor controle van username en password
            bool isCorrect = false;
            // string die input opvangt voor de username
            string tempUser;
            // string die input opvangt voor password
            string tempPass = "";
            // string die individuele Key inputs voor het password opvangt
            string passInput;

            Console.Clear();

            // input
            // schrijft op de eerste en derde lijn (voordat de input gevraagd wordt)
            Console.WriteLine("APOTHEEK JEF: DATABASE\n");
            Console.WriteLine("Geef uw username in:\n");
            Console.WriteLine("Geef uw wachtwoord in");
            // schrijf de username op tweede lijn
            // username is niet case sensitive, we slaan op als uppercase
            Console.SetCursorPosition(0, 3);
            tempUser = Console.ReadLine().ToUpper();
            // schrijf de password op de vierde lijn
            Console.SetCursorPosition(0, 5);
            while (true) // infinite loop, we gaan hier enkel uit via een break
            {
                // we lezen 1 key in zonder deze te tonen in de console en checken deze voor verschillende opties
                passInput = Console.ReadKey(false).Key.ToString();
                if (passInput == "Enter") // als de user enter indrukt, is hij klaar met het password in te geven => validatie
                {
                    break;
                }
                if (passInput == "Backspace") // als de user backspace indrukt, wil hij het vorige karakter wissen
                {
                    // we vervangen tempPass door tempPass zonder het laatste karakter
                    tempPass = tempPass.Substring(0, tempPass.Length - 1);
                    // we verwijderen 1 *
                    Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop);
                    Console.Write(" ");
                }
                if (passInput.Length == 1) // als de lengte van de Key 1 is, is dit een letter
                {
                    // voeg het karakter toe aan de tempPass
                    tempPass += passInput;

                }
                // we tonen een aantal *s gelijk aan het aantal karakters in tempPass
                Console.SetCursorPosition(0, Console.CursorTop);
                for (int i = 0; i < tempPass.Length; i++)
                {
                    Console.Write("*");
                }
            }

            if (tempUser == username && tempPass == password) // validatie user input met gegeven strings
            {
                isCorrect = true;
            }
            Console.WriteLine();
            return isCorrect; // geeft terug of de login succesvol was of niet
        }

        public static int MenuSelectie(int nrOptions, string option1, string option2, string option3 = "", string option4 = "", string option5 = "", string option6 = "", string option7 = "", string option8 = "", string option9 = "", string option0 = "")
        {
            // algemene applicatie om een keuzemenu te maken die navigeerbaar is met arrow keys

            // locale variabelen
            // cursor start altijd op positie 1
            int cursorPos = 1;
            // startPos is de lijn waarop het menu moet beginnen: vanaf hier wordt er gerefreshed
            int startPos = Console.CursorTop;
            // menuNummer geeft terug wat de user gekozen heeft (-1 indien niet gekozen)
            int menuNummer = -1;
            bool menuGekozen = false;
            // opslag voor Key events
            string menuInput;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.SetCursorPosition(0, startPos); // elke loop begint te tonen op dezelfde regel
            Console.WriteLine("1. " + option1.ToUpper());
            Console.ResetColor(); // kleurreset
            Console.WriteLine("2. " + option2.ToUpper());
            if (nrOptions >= 3) // opties 3+ zijn optioneel, moeten dus enkel getoond worden als die er zijn
            {
                Console.WriteLine("3. " + option3.ToUpper());
                if (nrOptions >= 4)
                {
                    Console.WriteLine("4. " + option4.ToUpper());
                    if (nrOptions >= 5)
                    {
                        Console.WriteLine("5. " + option5.ToUpper());
                        if (nrOptions >= 6)
                        {
                            Console.WriteLine("6. " + option6.ToUpper());
                            if (nrOptions >= 7)
                            {
                                Console.WriteLine("7. " + option7.ToUpper());
                                if (nrOptions >= 8)
                                {
                                    Console.WriteLine("8. " + option8.ToUpper());
                                    if (nrOptions >= 9)
                                    {
                                        Console.WriteLine("9. " + option9.ToUpper());
                                        if (nrOptions >= 10)
                                        {
                                            Console.WriteLine("0. " + option0.ToUpper());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            while (!menuGekozen) // menuGekozen is false zolang de user geen optie kiest
            {
                // navigeer het menu via keyboard inputs
                menuInput = Console.ReadKey(false).Key.ToString();
                switch (menuInput)
                {
                    case "UpArrow":
                        if (cursorPos > 1)
                        {
                            cursorPos--;
                        }
                        else
                        {
                            cursorPos = nrOptions;
                        }
                        break;
                    case "DownArrow":
                        if (cursorPos < nrOptions)
                        {
                            cursorPos++;
                        }
                        else
                        {
                            cursorPos = 1;
                        }
                        break;
                    // als de user zijn keypad gebruikt, kies automatisch de overeenkomstige optie
                    case "NumPad1":
                    case "NumPad2":
                    case "NumPad3":
                    case "NumPad4":
                    case "NumPad5":
                    case "NumPad6":
                    case "NumPad7":
                    case "NumPad8":
                    case "NumPad9":
                    case "NumPad0":
                        if (Convert.ToInt32(menuInput.Substring(6, 1)) > nrOptions)
                        {
                            break;
                        }
                        cursorPos = Convert.ToInt32(menuInput.Substring(6, 1));
                        menuNummer = Convert.ToInt32(menuInput.Substring(6, 1));
                        menuGekozen = true;
                        break;
                    // als de user op Enter drukt, geef de highlighted optie door aan menuNummer en beeindig de menuselectie
                    case "Enter":
                        menuNummer = cursorPos;
                        menuGekozen = true;
                        break;
                    // als de user op Escape drukt, ga uit het menu (returnwaarde is dan -1)
                    case "Escape":
                        menuGekozen = true;
                        break;
                }

                // toon alle opties en kleur de geselecteerde optie in groen
                if (cursorPos == 1) // als de cursor op deze positie staat
                {
                    // kleur de text groen
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                Console.SetCursorPosition(0, startPos); // elke loop begint te tonen op dezelfde regel
                Console.WriteLine("1. " + option1.ToUpper());
                Console.ResetColor(); // kleurreset voor als de kleur nog op groen staat
                if (cursorPos == 2) // idem aan optie 1
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                Console.WriteLine("2. " + option2.ToUpper());
                Console.ResetColor();
                if (nrOptions >= 3) // opties 3+ zijn optioneel, moeten dus enkel getoond worden als die er zijn
                {
                    if (cursorPos == 3)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                    }
                    Console.WriteLine("3. " + option3.ToUpper());
                    Console.ResetColor();
                    if (nrOptions >= 4)
                    {
                        if (cursorPos == 4)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                        }
                        Console.WriteLine("4. " + option4.ToUpper());
                        Console.ResetColor();
                        if (nrOptions >= 5)
                        {
                            if (cursorPos == 5)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                            }
                            Console.WriteLine("5. " + option5.ToUpper());
                            Console.ResetColor();
                            if (nrOptions >= 6)
                            {
                                if (cursorPos == 6)
                                {
                                    Console.ForegroundColor = ConsoleColor.Green;
                                }
                                Console.WriteLine("6. " + option6.ToUpper());
                                Console.ResetColor();
                                if (nrOptions >= 7)
                                {
                                    if (cursorPos == 7)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Green;
                                    }
                                    Console.WriteLine("7. " + option7.ToUpper());
                                    Console.ResetColor();
                                    if (nrOptions >= 8)
                                    {
                                        if (cursorPos == 8)
                                        {
                                            Console.ForegroundColor = ConsoleColor.Green;
                                        }
                                        Console.WriteLine("8. " + option8.ToUpper());
                                        Console.ResetColor();
                                        if (nrOptions >= 9)
                                        {
                                            if (cursorPos == 9)
                                            {
                                                Console.ForegroundColor = ConsoleColor.Green;
                                            }
                                            Console.WriteLine("9. " + option9.ToUpper());
                                            Console.ResetColor();
                                            if (nrOptions >= 10)
                                            {
                                                if (cursorPos == 10)
                                                {
                                                    Console.ForegroundColor = ConsoleColor.Green;
                                                }
                                                Console.WriteLine("0. " + option0.ToUpper());
                                                Console.ResetColor();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //Console.Clear();
            return menuNummer;
        }

        public static void NieuwMedicijn(ref int aantalMedicijnen, ref int indexID, ref int[] medID, ref string[] medNaam, ref string[] medType, ref string[] medLev, ref int[] medStock, ref double[] medBarcode, ref double[] medAKP, ref double[] medVKP, ref double[] medTemp, ref bool[] medVS, ref bool[] medTB)
        {
            // toont de menu om een nieuw medicijn toe te voegen

            bool opnieuwMedKiezen = true;
            bool bestaandeBarcode;
            while (opnieuwMedKiezen) // wil de user (nog) een nieuw medicijn toevoegen? JA => in de loop
            {
                try // user input, dus gegevensvalidatie
                {

                    Console.WriteLine("Geef de naam van het nieuwe medicijn: ");
                    string tempNaam = Console.ReadLine();
                    if (tempNaam.Length > 25) // anders fouten in uitlezen/wegschrijven bestanden
                    {
                        Console.WriteLine("Naam mag maximaal 25 karakters lang zijn!");
                        break; // indien er een fout is, stop met gegevens opvragen en begin evt opnieuw
                    }

                    Console.WriteLine("Geef het type van het nieuwe medicijn: ");
                    string tempType = Console.ReadLine();
                    if (tempType.Length > 25) // anders fouten in uitlezen/wegschrijven bestanden
                    {
                        Console.WriteLine("Type mag maximaal 25 karakters lang zijn!");
                        break; // indien er een fout is, stop met gegevens opvragen en begin evt opnieuw
                    }

                    Console.WriteLine("Geef de leverancier in van het nieuwe medicijn: ");
                    string tempLev = Console.ReadLine();
                    if (tempLev.Length > 25) // anders fouten in uitlezen/wegschrijven bestanden
                    {
                        Console.WriteLine("Leverancier mag maximaal 25 karakters lang zijn!");
                        break; // indien er een fout is, stop met gegevens opvragen en begin evt opnieuw
                    }

                    Console.WriteLine("Geef de stock van het nieuwe medicijn: ");
                    int tempStock = Convert.ToInt32(Console.ReadLine());
                    if (tempStock < 0) // negatieve stock bestaat niet (als het systeem goed werkt)
                    {
                        Console.WriteLine("Stockaantal moet minstens 0 zijn!");
                        break; // indien er een fout is, stop met gegevens opvragen en begin evt opnieuw
                    }

                    bestaandeBarcode = false;
                    Console.WriteLine("Geef de barcode van het nieuwe medicijn: ");
                    double tempBarcode = Convert.ToDouble(Console.ReadLine());
                    if (tempBarcode > 9999999999999 || tempBarcode < 1000000000000) // EAN code moet uit 13 cijfers bestaan
                    {
                        Console.WriteLine("Barcode moet uit 13 cijfers bestaan!");
                        break; // indien er een fout is, stop met gegevens opvragen en begin evt opnieuw
                    }
                    for (int i = 0; i < aantalMedicijnen; i++)
                    {
                        if (Array.IndexOf(medBarcode, tempBarcode) != -1)
                        {
                            bestaandeBarcode = true;
                        }
                    }
                    if (bestaandeBarcode)
                    {
                        Console.WriteLine("Barcode is al in gebruik!");
                        break;
                    }

                    Console.WriteLine("Geef de aankooppijs per stuk van het nieuwe medicijn: ");
                    double tempAkp = Convert.ToDouble(Console.ReadLine());
                    if (tempAkp < 0) // prijzen zijn positief
                    {
                        Console.WriteLine("Aankoopprijs moet positief zijn!");
                        break; // indien er een fout is, stop met gegevens opvragen en begin evt opnieuw
                    }

                    Console.WriteLine("Geef de verkoopprijs per stuk van het nieuwe medicijn: ");
                    double tempVkp = Convert.ToDouble(Console.ReadLine());
                    if (tempVkp < 0) // prijzen zijn positief
                    {
                        Console.WriteLine("Verkoopprijs moet positief zijn!");
                        break; // indien er een fout is, stop met gegevens opvragen en begin evt opnieuw
                    }

                    Console.WriteLine("Geef de stockagetemperatuur van het nieuwe medicijn: ");
                    double tempTemp = Convert.ToDouble(Console.ReadLine());
                    if (Math.Abs(tempTemp) > 50) // Jef zijn koelkast gaat maar to -50 C, en hij kan ook niet meer stoken dan +50 C
                    {
                        Console.WriteLine("Stockagetemperatuur moet tussen -50 en +50 graden Celsius zijn!");
                        break; // indien er een fout is, stop met gegevens opvragen en begin evt opnieuw
                    }

                    Console.WriteLine("Heeft men een voorschrift nodig om dit medicijn te kopen?");
                    bool tempVS = false;
                    if (MenuSelectie(2, "ja", "nee") == 1)
                    {
                        tempVS = true;
                    }

                    Console.WriteLine("Kan het medicijn (deels) worden terugbetaald door de ziekenkas?");
                    bool tempTB = false;
                    if (MenuSelectie(2, "ja", "nee") == 1)
                    {
                        tempTB = true;
                    }

                    // deze code wordt enkel uitgevoerd als er geen enkele foutmelding is gegeven
                    // increment aantal medicijnen, resize arrays zodat ze 1 groter zijn, sla de ingegeven data op in de juiste syntax
                    indexID++;
                    aantalMedicijnen++;
                    Array.Resize(ref medID, aantalMedicijnen);
                    medID[aantalMedicijnen - 1] = indexID;
                    Array.Resize(ref medNaam, aantalMedicijnen);
                    medNaam[aantalMedicijnen - 1] = tempNaam.Replace(" ", "_");
                    Array.Resize(ref medType, aantalMedicijnen);
                    medType[aantalMedicijnen - 1] = tempType.Replace(" ", "_");
                    Array.Resize(ref medLev, aantalMedicijnen);
                    medLev[aantalMedicijnen - 1] = tempLev.Replace(" ", "_");
                    Array.Resize(ref medStock, aantalMedicijnen);
                    medStock[aantalMedicijnen - 1] = tempStock;
                    Array.Resize(ref medBarcode, aantalMedicijnen);
                    medBarcode[aantalMedicijnen - 1] = tempBarcode;
                    Array.Resize(ref medAKP, aantalMedicijnen);
                    medAKP[aantalMedicijnen - 1] = tempAkp;
                    Array.Resize(ref medVKP, aantalMedicijnen);
                    medVKP[aantalMedicijnen - 1] = tempVkp;
                    Array.Resize(ref medTemp, aantalMedicijnen);
                    medTemp[aantalMedicijnen - 1] = tempTemp;
                    Array.Resize(ref medVS, aantalMedicijnen);
                    medVS[aantalMedicijnen - 1] = tempVS;
                    Array.Resize(ref medTB, aantalMedicijnen);
                    medTB[aantalMedicijnen - 1] = tempTB;

                }
                catch
                {
                    Console.WriteLine("Ongeldige invoer!");
                }
                // evt opnieuw beginnen
                Console.Clear();
                Console.WriteLine("Wil je een nieuw medicijn toevoegen Ja of Nee?");
                if (MenuSelectie(2, "ja", "nee") == 1)
                {
                    opnieuwMedKiezen = true;
                }
                else
                {
                    opnieuwMedKiezen = false;
                }
                Console.Clear();
            }
        }

        public static void WijzigMedicijn(int aantalMedicijnen, ref int[] medID, ref string[] medNaam, ref string[] medType, ref string[] medLev, ref int[] medStock, ref double[] medBarcode, ref double[] medAKP, ref double[] medVKP, ref double[] medTemp, ref bool[] medVS, ref bool[] medTB)
        {
            // toont de menu om een medicijn te wijzigen

            // een nieuw medicijn kiezen om te veranderen?
            bool opnieuwMedKiezen = true;
            // index van het medicijn in de parameterarrays
            int medicijnNr;
            // een nieuwe parameter van hetzelfe medicijn wijzigen?
            bool opnieuwParamKiezen;
            // opslag voor keuze van te wijzigen parameter
            int submenuNummer;
            // exit naar hoofdmenu?
            bool terugHoofdMenu = false;

            while (opnieuwMedKiezen)
            {
                Console.Clear();
                DisplayAlleMedicijnen(aantalMedicijnen, medID, medNaam, medType, medLev, medStock, medBarcode, medAKP, medVKP, medTemp, medVS, medTB);
                try
                {
                    Console.Write("Geef de ID van het medicijn dat je wil aanpassen: ");
                    medicijnNr = Array.IndexOf(medID, Convert.ToInt32(Console.ReadLine()));
                }
                catch
                {
                    Console.WriteLine("Ongeldige invoer!");
                    medicijnNr = -1;
                }
                Console.Clear();
                if (medicijnNr > -1) // als de index niet -1 is, zit het medicijn in de arrays, dus we kunnen wijzigen
                {
                    opnieuwParamKiezen = true;
                    while (opnieuwParamKiezen)
                    {
                        Console.Clear();
                        DisplayMedicijn(medID, medNaam, medType, medLev, medStock, medBarcode, medAKP, medVKP, medTemp, medVS, medTB, medicijnNr);
                        Console.WriteLine("Welke parameter wil je veranderen?");
                        submenuNummer = MenuSelectie(10, "naam", "type", "leverancier", "stockaantal", "barcode", "aankoopprijs", "verkoopprijs", "stockagetemperatuur", "voorschrift?", "terugbetaald?");
                        try
                        {
                            // user input => gegevensvalidatie
                            switch (submenuNummer)
                            {
                                case 1:
                                    // analoog aan NieuwMedicijn
                                    Console.Write("Geef het nieuwe naam in: ");
                                    string tempNaam = Console.ReadLine().Replace(" ", "_"); // sla input op in temporary variable
                                    if (tempNaam.Length > 25) // max lengte bepaald door saves en loads uit bestanden
                                    {
                                        Console.WriteLine("Type mag maximaal 25 karakters lang zijn!");
                                        break;
                                    }
                                    if (tempNaam != "")
                                    {
                                        medNaam[medicijnNr] = tempNaam.Replace(" ", "_");
                                    }
                                    break;
                                case 2:
                                    Console.Write("Geef het nieuwe type in: ");
                                    string tempType = Console.ReadLine().Replace(" ", "_");
                                    if (tempType.Length > 25)
                                    {
                                        Console.WriteLine("Type mag maximaal 25 karakters lang zijn!");
                                        break;
                                    }
                                    if (tempType != "")
                                    {
                                        medType[medicijnNr] = tempType.Replace(" ", "_");
                                    }
                                    break;
                                case 3:
                                    Console.Write("Geef de nieuwe leverancier in: ");
                                    string tempLev = Console.ReadLine().Replace(" ", "_");
                                    if (tempLev.Length > 25)
                                    {
                                        Console.WriteLine("Leverancier mag maximaal 25 karakters lang zijn!");
                                        break;
                                    }
                                    if (tempLev != "")
                                    {
                                        medLev[medicijnNr] = tempLev.Replace(" ", "_");
                                    }
                                    break;
                                case 4:
                                    Console.Write("Geef het nieuwe stockaantal in: ");
                                    int tempStock = Convert.ToInt32(Console.ReadLine());
                                    if (tempStock < 0)
                                    {
                                        Console.WriteLine("Stockaantal moet minstens 0 zijn!");
                                        break;
                                    }
                                    medStock[medicijnNr] = tempStock;
                                    break;
                                case 5:
                                    bool bestaandeBarcode = false;
                                    Console.Write("Geef de nieuwe barcode in: ");
                                    double tempBarcode = Convert.ToDouble(Console.ReadLine());
                                    if (tempBarcode > 9999999999999 || tempBarcode < 1000000000000)
                                    {
                                        Console.WriteLine("Barcode moet uit 13 cijfers bestaan!");
                                        break;
                                    }
                                    for (int i = 0; i < medBarcode.Length; i++)
                                    {
                                        if (Array.IndexOf(medBarcode, tempBarcode) != -1)
                                        {
                                            bestaandeBarcode = true;
                                        }
                                    }
                                    if (bestaandeBarcode)
                                    {
                                        Console.WriteLine("Barcode is al in gebruik!");
                                        break;
                                    }
                                    medBarcode[medicijnNr] = tempBarcode;
                                    break;
                                case 6:
                                    Console.Write("Geef de nieuwe aankoopprijs in: ");
                                    double tempAkp = Convert.ToDouble(Console.ReadLine());
                                    if (tempAkp < 0)
                                    {
                                        Console.WriteLine("Aankoopprijs moet positief zijn!");
                                        break;
                                    }
                                    medAKP[medicijnNr] = tempAkp;
                                    break;
                                case 7:
                                    Console.Write("Geef de nieuwe verkoopprijs in: ");
                                    double tempVkp = Convert.ToDouble(Console.ReadLine());
                                    if (tempVkp < 0)
                                    {
                                        Console.WriteLine("Verkoopprijs moet positief zijn!");
                                        break;
                                    }
                                    medVKP[medicijnNr] = tempVkp;
                                    break;
                                case 8:
                                    Console.Write("Geef de nieuwe stockagetemperatuur in: ");
                                    double tempTemp = Convert.ToDouble(Console.ReadLine());
                                    if (Math.Abs(tempTemp) > 50)
                                    {
                                        Console.WriteLine("Stockagetemperatuur moet tussen -50 en +50 graden Celsius zijn!");
                                        break;
                                    }
                                    medTemp[medicijnNr] = tempTemp;
                                    break;
                                case 9:
                                    Console.WriteLine("Heeft men een voorschrift nodig om dit medicijn te kopen?");
                                    if (MenuSelectie(2, "ja", "nee") == 1)
                                    {
                                        medVS[medicijnNr] = true;
                                    }
                                    else
                                    {
                                        medVS[medicijnNr] = false;
                                    }
                                    break;
                                case 0:
                                    Console.WriteLine("Kan het medicijn (deels) worden terugbetaald door de ziekenkas?");
                                    if (MenuSelectie(2, "ja", "nee") == 1)
                                    {
                                        medTB[medicijnNr] = true;
                                    }
                                    else
                                    {
                                        medTB[medicijnNr] = false;
                                    }
                                    break;
                                default:
                                    terugHoofdMenu = true;
                                    break;
                            }
                            Console.Clear();
                        }
                        catch // error in de inputs
                        {
                            Console.WriteLine("Ongeldige invoer!");
                        }
                        if (terugHoofdMenu) // user wil naar hoofdmenu
                        {
                            break;
                        }
                        Console.WriteLine($"Wil je nog een parameter van {medNaam[medicijnNr].Replace(" ", "_")} veranderen?");
                        if (MenuSelectie(2, "ja", "nee") == 1)
                        {
                            opnieuwParamKiezen = true;
                        }
                        else
                        {
                            opnieuwParamKiezen = false;
                        }
                        Console.Clear();
                    }
                }
                else // IndexOf geeft -1 => geen gekend medicijn
                {
                    Console.WriteLine("Dit medicijn staat niet in de database.");
                }
                Console.WriteLine("Wil je een ander medicijn wijzigen?");
                if (MenuSelectie(2, "ja", "nee") == 1)
                {
                    opnieuwMedKiezen = true;
                }
                else
                {
                    opnieuwMedKiezen = false;
                }
                Console.Clear();
            }
        }

        public static void VerwijderMedicijn(ref int aantalMedicijnen, ref int[] medID, ref string[] medNaam, ref string[] medType, ref string[] medLev, ref int[] medStock, ref double[] medBarcode, ref double[] medAKP, ref double[] medVKP, ref double[] medTemp, ref bool[] medVS, ref bool[] medTB, int[] resMedID)
        {
            // toont de menu om een medicijn te verwijderen

            // nog een medicijn verwijderen?
            bool medicijnRemove = true;
            // index van het te verwijderen medicijn in de arrays
            int medicijnNr;

            while (medicijnRemove)
            {
                Console.Clear();
                DisplayAlleMedicijnen(aantalMedicijnen, medID, medNaam, medType, medLev, medStock, medBarcode, medAKP, medVKP, medTemp, medVS, medTB);
                Console.Write("Geef de ID van het medicijn dat je wil verwijderen: ");
                medicijnNr = Array.IndexOf(medID, Convert.ToInt32(Console.ReadLine())); // zoek input in de ID array
                if (medicijnNr > -1)
                {
                    if (Array.IndexOf(resMedID, medID[medicijnNr]) == -1)
                    {
                        // vraag de user om bevestiging
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"U zal het medicijn {medNaam[medicijnNr]}, type {medType[medicijnNr].Replace("_", " ")} verwijderen.\nBent u zeker? De data kan niet meer hersteld worden!");
                        Console.ResetColor();
                        if (MenuSelectie(2, "ja", "nee") == 1)
                        {
                            Console.WriteLine($"Het medicijn {medNaam[medicijnNr]}, type {medType[medicijnNr].Replace("_", " ")} is verwijderd");
                            if (medicijnNr != aantalMedicijnen - 1) // als het te verwijderen medicijn niet op de laatste index staat
                            {
                                for (int i = medicijnNr; i < aantalMedicijnen - 1; i++)
                                {
                                    // overschrijf het te verwijderen medicijn met die in de volgende index, doe dit voor elke index zodat de laatste plaats overbodig is
                                    medID[i] = medID[i + 1];
                                    medNaam[i] = medNaam[i + 1];
                                    medType[i] = medType[i + 1];
                                    medLev[i] = medLev[i + 1];
                                    medStock[i] = medStock[i + 1];
                                    medBarcode[i] = medBarcode[i + 1];
                                    medAKP[i] = medAKP[i + 1];
                                    medVKP[i] = medVKP[i + 1];
                                    medTemp[i] = medTemp[i + 1];
                                    medVS[i] = medVS[i + 1];
                                    medTB[i] = medTB[i + 1];
                                }
                            }
                            // decrementeeer het aantal medicijnen
                            // resize de arrays, ze laten automatisch de laatste index vallen
                            aantalMedicijnen--;
                            Array.Resize(ref medID, aantalMedicijnen);
                            Array.Resize(ref medNaam, aantalMedicijnen);
                            Array.Resize(ref medType, aantalMedicijnen);
                            Array.Resize(ref medLev, aantalMedicijnen);
                            Array.Resize(ref medStock, aantalMedicijnen);
                            Array.Resize(ref medBarcode, aantalMedicijnen);
                            Array.Resize(ref medAKP, aantalMedicijnen);
                            Array.Resize(ref medVKP, aantalMedicijnen);
                            Array.Resize(ref medTemp, aantalMedicijnen);
                            Array.Resize(ref medVS, aantalMedicijnen);
                            Array.Resize(ref medTB, aantalMedicijnen);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Dit medicijn is gereserveerd. Verwijder de reservatie vooraleer je dit medicijn verwijderd.");
                    }
                }
                else
                {
                    Console.WriteLine("Dit medicijn staat niet in de database.");
                }
                Console.WriteLine("Wil je een ander medicijn verwijderen?");
                if (MenuSelectie(2, "ja", "nee") == 1)
                {
                    medicijnRemove = true;
                }
                else
                {
                    medicijnRemove = false;
                }
                Console.WriteLine();
            }
        }

        public static void DisplayAlleMedicijnen(int aantalMedicijnen, int[] medID, string[] medNaam, string[] medType, string[] medLev, int[] medStock, double[] medBarcode, double[] medAKP, double[] medVKP, double[] medTemp, bool[] medVS, bool[] medTB)
        {
            // toont de volledige lijst van alle medicijnen

            // header
            Console.WriteLine("ID".PadRight(4) + " | " + "Naam medicijn".PadRight(25) + " | " + "Type/Vorm".PadRight(25) + " | " + "Leverancier".PadRight(25) + " | " + "Aantal".PadRight(6) + " | " + "Barcode".PadRight(13) + " | " + "AKP".PadRight(8)
                    + " | " + "VKP".PadRight(8) + " | " + "Temperatuur" + " | " + "Voorschrift?" + " | " + "Terugbetaald?");
            Console.WriteLine("".PadRight(180, '_') + "\n");
            for (int i = 0; i < aantalMedicijnen; i++) // toon per medicijn alle parameters op een regel met correcte spacing
            {
                Console.WriteLine(medID[i].ToString().PadRight(4) + " | " + medNaam[i].PadRight(25).Replace("_", " ") + " | " + medType[i].PadRight(25).Replace("_", " ") + " | " + medLev[i].PadRight(25).Replace("_", " ") + " | " + medStock[i].ToString().PadLeft(6) + " | " + medBarcode[i].ToString()
                    + " | " + medAKP[i].ToString("0.00").PadLeft(8) + " | " + medVKP[i].ToString("0.00").PadLeft(8) + " | " + (medTemp[i].ToString("0.0") + Convert.ToChar(176).ToString().PadLeft(2)
                    + "C").PadLeft(11) + " | " + medVS[i].ToString().PadLeft(12) + " | " + medTB[i].ToString().PadLeft(13));
            }
            Console.WriteLine();
        }

        public static void DisplayMedicijn(int[] medID, string[] medNaam, string[] medType, string[] medLev, int[] medStock, double[] medBarcode, double[] medAKP, double[] medVKP, double[] medTemp, bool[] medVS, bool[] medTB, int givenID = -1)
        {
            // toon een medicijn op basis van naam of barcode

            // index van het weer te geven medicijn
            int medicijnNr;
            // nog een medicijn tonen?
            bool opnieuwZoeken = true;

            while (opnieuwZoeken)
            {
                // keuze tussen zoeken op naam of barcode
                if (givenID == -1)
                {
                    Console.WriteLine("Wil je zoeken op ID op barcode?");
                    try
                    {
                        // user input => gegevensvalidatie
                        if (MenuSelectie(2, "ID", "barcode") == 1)
                        {
                            Console.Write("Geef de ID van het medicijn dat je wil aanpassen: ");
                            medicijnNr = Array.IndexOf(medID, Convert.ToInt32(Console.ReadLine()));
                        }
                        else
                        {
                            Console.Write("Geef de barcode in van het medicijn dat je wil aanpassen: ");
                            medicijnNr = Array.IndexOf(medBarcode, Convert.ToDouble(Console.ReadLine()));
                        }

                    }
                    catch
                    {
                        Console.WriteLine("Ongeldige invoer!");
                        medicijnNr = -1;
                    }
                }
                else
                {
                    medicijnNr = givenID;
                }

                if (medicijnNr > -1)
                {
                    // als input gevonden is in de arrays, toon de parameters
                    Console.Clear();
                    Console.WriteLine("ID".PadRight(4) + " | " + "Naam medicijn".PadRight(25) + " | " + "Type/Vorm".PadRight(25) + " | " + "Leverancier".PadRight(25) + " | " + "Aantal".PadRight(6) + " | " + "Barcode".PadRight(13) + " | " + "AKP".PadRight(8)
                    + " | " + "VKP".PadRight(8) + " | " + "Temperatuur" + " | " + "Voorschrift?" + " | " + "Terugbetaald?");
                    Console.WriteLine("".PadRight(180, '_') + "\n");
                    Console.WriteLine(medID[medicijnNr].ToString().PadRight(4) + " | " + medNaam[medicijnNr].PadRight(25).Replace("_", " ") + " | " + medType[medicijnNr].PadRight(25).Replace("_", " ") + " | " + medLev[medicijnNr].PadRight(25).Replace("_", " ")
                    + " | " + medStock[medicijnNr].ToString().PadLeft(6) + " | " + medBarcode[medicijnNr].ToString() + " | " + medAKP[medicijnNr].ToString("0.00").PadLeft(8) + " | "
                    + medVKP[medicijnNr].ToString("0.00").PadLeft(8) + " | " + (medTemp[medicijnNr].ToString("0.0") + Convert.ToChar(176).ToString().PadLeft(2) + "C").PadLeft(11) + " | "
                    + medVS[medicijnNr].ToString().PadLeft(12) + " | " + medTB[medicijnNr].ToString().PadLeft(13) + "\n");
                }
                else
                {
                    Console.WriteLine("Dit medicijn zit niet in de database.");
                }
                if (givenID == -1)
                {
                    Console.WriteLine("Wil je nog een medicijn opzoeken?");
                    if (MenuSelectie(2, "ja", "nee") == 1)
                    {
                        opnieuwZoeken = true;
                    }
                    else
                    {
                        opnieuwZoeken = false;
                    }
                    Console.Clear();
                }
                else
                {
                    break;
                }
            }
        }

        public static void SaveMedicijnen(int aantalMedicijnen, int indexID, int[] medID, string[] medNaam, string[] medType, string[] medLev, int[] medStock, double[] medBarcode, double[] medAKP, double[] medVKP, double[] medTemp, bool[] medVS, bool[] medTB)
        {
            // wegschrijven van arrays naar bestand
            using (StreamWriter opslag = new StreamWriter("medicijnen.txt"))
            {
                // lijn 0: enkel het aantal medicijnen
                opslag.WriteLine(aantalMedicijnen.ToString().PadRight(4) + "|" + indexID.ToString().PadRight(4));
                for (int i = 0; i < aantalMedicijnen; i++)
                {
                    // op elke lijn staan alle parameters van 1 medicijn 
                    opslag.Write(medID[i].ToString().PadRight(4) + "|");
                    opslag.Write(medNaam[i].Replace(" ", "_").PadRight(25) + "|");
                    opslag.Write(medType[i].Replace(" ", "_").PadRight(25) + "|");
                    opslag.Write(medLev[i].Replace(" ", "_").PadRight(25) + "|");
                    opslag.Write(medStock[i].ToString().PadRight(10) + "|");
                    opslag.Write(medBarcode[i].ToString() + "|");
                    opslag.Write(medAKP[i].ToString().PadRight(8) + "|");
                    opslag.Write(medVKP[i].ToString().PadRight(8) + "|");
                    opslag.Write(medTemp[i].ToString().PadRight(6) + "|");
                    opslag.Write(medVS[i].ToString().PadRight(5) + "|");
                    opslag.WriteLine(medTB[i].ToString().PadRight(5));
                }
            }
        }

        public static void SaveReservaties(int aantalReservaties, string[] resKlant, int[] resMedID, int[] resAantal, DateTime[] resDatum)
        {
            // analoog aan SaveMedicijnen
            using (StreamWriter opslag = new StreamWriter("reservaties.txt"))
            {
                opslag.WriteLine(aantalReservaties);
                for (int i = 0; i < aantalReservaties; i++)
                {
                    opslag.Write(resKlant[i].Replace(" ", "_").PadRight(25) + "|");
                    opslag.Write(resMedID[i].ToString().PadRight(4) + "|");
                    opslag.Write(resAantal[i].ToString().PadRight(4) + "|");
                    opslag.WriteLine(resDatum[i].ToString().Substring(0, 10));
                }
            }
        }

        public static void ReservatieAanmaken(ref int aantalReservaties, ref string[] resKlant, ref int[] resMedID, ref int[] resAantal, ref DateTime[] resDatum, ref int[] medID, ref int[] medStock)
        {
            // menu om een nieuwe resevatie in te geven en in de arrays op te slaan

            // wil een nieuwe klant een reservatie doen?
            bool nieuwResKlant = true;
            // wil dezelfde klant een reservatie doen?
            bool nieuwReservatie = true;
            // index van het te reserveren medicijn
            int resNr;
            int tempMed = 0;
            int tempStock;

            while (nieuwReservatie)
            {
                Console.WriteLine("Geef de naam van de klant in: ");
                string tempKlant = Console.ReadLine().Replace(" ", "_");
                while (nieuwResKlant)
                {
                    Console.WriteLine("Geef de ID van het medicijn in: ");
                    try
                    {
                        tempMed = Convert.ToInt32(Console.ReadLine());
                        resNr = Array.IndexOf(medID, tempMed); // controle of medicijn bestaat
                    }
                    catch
                    {
                        Console.WriteLine("Ongeldige invoer!");
                        resNr = -1;
                    }
                    if (resNr > -1)
                    {
                        Console.WriteLine($"Er zijn {medStock[resNr]} stuks hiervan in stock.");
                        Console.WriteLine("Hoeveel stuks wilt u reserveren? ");
                        try
                        {
                            tempStock = Convert.ToInt32(Console.ReadLine()); // aantal te reserveren
                            if (tempStock <= medStock[resNr]) // is er genoeg stock?
                            {
                                medStock[resNr] -= tempStock; // neem uit de stock
                                aantalReservaties++; // een nieuwe reservatie
                                                     // maak een nieuwe plaats in elke parameterarray en vul in
                                Array.Resize(ref resKlant, aantalReservaties);
                                Array.Resize(ref resMedID, aantalReservaties);
                                Array.Resize(ref resAantal, aantalReservaties);
                                Array.Resize(ref resDatum, aantalReservaties);
                                resKlant[aantalReservaties - 1] = tempKlant;
                                resMedID[aantalReservaties - 1] = tempMed;
                                resAantal[aantalReservaties - 1] = tempStock;
                                resDatum[aantalReservaties - 1] = DateTime.Now.Date; // vult automatisch de datum van vandaag in
                            }
                            else
                            {
                                Console.WriteLine($"Niet genoeg in stock. Er zijn slechts {medStock[resNr]} stuks beschikbaar.");
                            }
                        }
                        catch
                        {
                            Console.WriteLine("Ongeldige invoer!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Dit medicijn zit niet in het databank.");
                    }

                    Console.WriteLine("Wilt de klant nog een medicijn reserveren? ");
                    if (MenuSelectie(2, "ja", "nee") == 1)
                    {
                        nieuwResKlant = true;
                    }
                    else
                    {
                        nieuwResKlant = false;
                    }
                    Console.Clear();
                }
                Console.WriteLine("Wilt een andere klant een reservatie maken? ");
                if (MenuSelectie(2, "ja", "nee") == 1)
                {
                    nieuwReservatie = true;
                }
                else
                {
                    nieuwReservatie = false;
                }
                Console.Clear();
            }
        }

        public static void ReservatieAfwerken(ref int aantalReservaties, ref string[] resKlant, ref int[] resMedID, ref int[] resAantal, ref DateTime[] resDatum, int[] medID, string[] medNaam, string[] medType)
        {
            // klant komt reservatie afhalen

            // opslag user input klantennaam
            string tempKlant;
            // opslag user input gereserveerd medicijn
            int tempMed;
            // wordt er nog een reservatie afgehaald?
            bool opnieuwAfw = true;
            bool resGevonden;

            while (opnieuwAfw)
            {
                DisplayReservaties(aantalReservaties, resKlant, resMedID, resAantal, resDatum, medID, medNaam, medType);
                Console.WriteLine();
                try
                {
                    Console.WriteLine("Geef de naam van de klant in: ");
                    tempKlant = Console.ReadLine().Replace(" ", "_");
                    Console.WriteLine("Geef de ID van het medicijn in: ");
                    tempMed = Convert.ToInt32(Console.ReadLine());
                    resGevonden = false;
                    for (int i = 0; i < aantalReservaties; i++) // loop over alle reservaties
                    {
                        if (resKlant[i] == tempKlant && resMedID[i] == tempMed) // kijk of de ingegeven klant het ingegeven medicijn heeft gereserveerd
                        {
                            for (int j = i; j < aantalReservaties - 1; j++) // overschrijf de reservatie en schuif alles op
                            {
                                resKlant[j] = resKlant[j + 1];
                                resMedID[j] = resMedID[j + 1];
                                resAantal[j] = resAantal[j + 1];
                                resDatum[j] = resDatum[j + 1];
                            }
                            // decrementeer aantal reservaties
                            // verkort de arrays met 1 plaats
                            aantalReservaties--;
                            Array.Resize(ref resKlant, aantalReservaties);
                            Array.Resize(ref resMedID, aantalReservaties);
                            Array.Resize(ref resAantal, aantalReservaties);
                            Array.Resize(ref resDatum, aantalReservaties);
                            resGevonden = true;
                            break; // indien een match gevonden is, stop met zoeken naar een match
                        }

                    }
                    if (resGevonden == false)
                    {
                        Console.WriteLine("Er is geen reservatie gevonden die voldoet aan de invoer.");
                    }
                }
                catch
                {
                    Console.WriteLine("Ongeldige invoer!");
                }

                Console.WriteLine("Wil je nog een reservatie afwerken? ");
                if (MenuSelectie(2, "ja", "nee") == 1)
                {
                    opnieuwAfw = true;
                }
                else
                {
                    opnieuwAfw = false;
                }
                Console.Clear();
            }
        }

        public static void ReservatieAnnuleren(ref int aantalReservaties, ref string[] resKlant, ref int[] resMedID, ref int[] resAantal, ref DateTime[] resDatum, ref int[] medStock, int[] medID, string[] medNaam, string[] medType)
        {
            // klant komt reservatie niet afhalen
            // quasi volledig analoog aan ReservatieAfwerken

            string tempKlant;
            int tempMed;
            bool opnieuwAnnul = true;



            while (opnieuwAnnul)
            {
                DisplayReservaties(aantalReservaties, resKlant, resMedID, resAantal, resDatum, medID, medNaam, medType);
                Console.WriteLine();
                try
                {
                    Console.WriteLine("Geef de naam van de klant in: ");
                    tempKlant = Console.ReadLine().Replace(" ", "_");
                    Console.WriteLine("Geef de ID van het medicijn in: ");
                    tempMed = Convert.ToInt32(Console.ReadLine());
                    for (int i = 0; i < aantalReservaties; i++)
                    {
                        if (resKlant[i] == tempKlant && resMedID[i] == tempMed)
                        {
                            medStock[Array.IndexOf(medID, tempMed)] += resAantal[i]; // steek de gereserveerde hoeveelheid terug in de stock
                            for (int j = i; j < aantalReservaties - 1; j++)
                            {
                                resKlant[j] = resKlant[j + 1];
                                resMedID[j] = resMedID[j + 1];
                                resAantal[j] = resAantal[j + 1];
                                resDatum[j] = resDatum[j + 1];
                            }
                            aantalReservaties--;
                            Array.Resize(ref resKlant, aantalReservaties);
                            Array.Resize(ref resMedID, aantalReservaties);
                            Array.Resize(ref resAantal, aantalReservaties);
                            Array.Resize(ref resDatum, aantalReservaties);
                            break;
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Ongeldige invoer!");
                }
                Console.WriteLine("Wil je nog een reservatie annuleren? ");
                if (MenuSelectie(2, "ja", "nee") == 1)
                {
                    opnieuwAnnul = true;
                }
                else
                {
                    opnieuwAnnul = false;
                }
            }
        }

        public static void DisplayReservaties(int aantalReservaties, string[] resKlant, int[] resMedID, int[] resAantal, DateTime[] resDatum, int[] medID, string[] medNaam, string[] medType)
        {
            // toon alle reservaties
            // reservaties worden in principe altijd chronologisch opgeslagen, dus print gewoon alles in volgorde

            int index;
            Console.WriteLine("Naam Klant".PadRight(25) + " | " + "ID".PadRight(4) + " | " + "Naam Medicijn".PadRight(25) + " | " + "Type Medicijn".PadRight(25) + " | " + "Aantal" + " | " + "Datum");
            Console.WriteLine("".PadRight(110, '_') + "\n");
            for (int i = 0; i < aantalReservaties; i++)
            {
                index = Array.IndexOf(medID, resMedID[i]);
                Console.WriteLine(resKlant[i].PadRight(25).Replace("_", " ") + " | " + resMedID[i].ToString().PadRight(4) + " | " + medNaam[index].PadRight(25).Replace("_", " ") + " | " + medType[index].PadRight(25).Replace("_", " ") + " | " + resAantal[i].ToString().PadLeft(6) + " | " + resDatum[i].ToString().Substring(0, 10));
            }


        }

        public static void DisplayKlant(int aantalReservaties, string[] resKlant, int[] resMedID, int[] resAantal, DateTime[] resDatum, int[] medID, string[] medNaam, string[] medType, string klant = "")
        {
            // opslag user input
            string naamKlant;
            int index;
            if (klant == "") // optioneel: geef een string mee die de userinput vervangt
            {
                // indien geen meegegeven naam, vraag user input
                Console.WriteLine("Geef de naam van de klant in: ");
                naamKlant = Console.ReadLine().Replace(" ", "_");
                Console.Clear();

            }
            else
            {
                naamKlant = klant;
            }
            Console.WriteLine("Naam Klant".PadRight(25) + " | " + "Naam Medicijn".PadRight(25) + " | " + "Type Medicijn".PadRight(25) + " | " + "Aantal" + " | " + "Datum");
            Console.WriteLine("".PadRight(110, '_') + "\n");
            for (int i = 0; i < aantalReservaties; i++)
            {
                // toon elke reservatie met een overeenkomstige naam

                if (naamKlant == resKlant[i])
                {
                    index = Array.IndexOf(medID, resMedID[i]);
                    Console.WriteLine(resKlant[i].PadRight(25).Replace("_", " ") + " | " + medNaam[index].PadRight(25).Replace("_", " ") + " | " + medType[index].PadRight(25).Replace("_", " ") + " | " + resAantal[i].ToString().PadLeft(6) + " | " + resDatum[i].ToString().Substring(0, 10));
                }
            }
        }
    }
}